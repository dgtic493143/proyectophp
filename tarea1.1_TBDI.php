<html> 
	<head> 
		<title>Piramide</title> 
	</head> 
	<style>
		.centrar{
			text-align: center;
		}
	</style>
	<body> 
		<?php
		$fila = 1;
		$altura = 30;
		
		echo '<div class="centrar">';
		while ($fila <= $altura) {

			$asteriscos = str_repeat("*", 1 + $fila - 1); 

			echo($asteriscos . "<br>");
		
			$fila++;
		}

        $fila = $altura - 1;
		while ($fila > 0) {
			$asteriscos = str_repeat("*", $fila);
			echo($asteriscos . "<br>");
			$fila--;
		}
        
		echo '</div>';
		?>
	</body>
</html> 