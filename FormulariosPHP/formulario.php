<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    header('Location: login.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
   
    $usuarios_registrados = isset($_SESSION['usuarios_registrados']) ? $_SESSION['usuarios_registrados'] : array();
    $nuevo_alumno = $_POST;
    $usuarios_registrados[] = $nuevo_alumno;

    
    $_SESSION['usuarios_registrados'] = $usuarios_registrados;

    header('Location: info.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
    <link rel="stylesheet" href="formulario.css">
</head>
<body>
    <h2>Formulario</h2>
    <form method="post" action="">
        <label for="num_cta">Número de cuenta:</label>
        <input type="number" name="num_cta" required><br>

        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" required><br>

        <label for="primer_apellido">Primer Apellido:</label>
        <input type="text" name="primer_apellido" required><br>

        <label for="segundo_apellido">Segundo Apellido:</label>
        <input type="text" name="segundo_apellido" required><br>

        <label for="genero">Género:</label>
        <select name="genero" required>
            <option value="M">Hombre</option>
            <option value="F">Mujer</option>
            <option value="O">Otro</option>
        </select><br>

        <label for="fec_nac">Fecha de Nacimiento:</label>
        <input type="date" name="fec_nac" required><br>

        <label for="contrasena">Contraseña:</label>
        <input type="password" name="contrasena" required><br>

        <input type="submit" value="Guardar Información">
    </form>

    <br>
    <a href="info.php">Ir a Información</a>
    <br>
    <a href="logout.php">Cerrar Sesión</a>
</body>
</html>
