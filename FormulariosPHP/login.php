<?php
session_start();

if (isset($_POST['submit'])) {
    $usuario_predefinido = array(
        'num_cta' => 1,
        'nombre' => 'Admin',
        'primer_apellido' => 'General',
        'genero' => 'O',
        'fec_nac' => '01/01/1990',
        'contrasena' => 'adminpass123.'
    );

    $num_cta = $_POST['num_cta'];
    $contrasena = $_POST['contrasena'];

    if ($num_cta == $usuario_predefinido['num_cta'] && $contrasena == $usuario_predefinido['contrasena']) {
        $_SESSION['usuario'] = $usuario_predefinido;
        header('Location: info.php');
        exit();
    } else {
        $error_message = 'Número de cuenta o contraseña incorrecta';
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="login.css">
</head>
<body>
    <form method="post" action="">
        <h2>Login</h2>
        <?php if (isset($error_message)) echo "<p>$error_message</p>"; ?>
        
        <label for="num_cta">Número de cuenta:</label>
        <input type="text" name="num_cta" required>

        <label for="contrasena">Contraseña:</label>
        <input type="password" name="contrasena" required>

        <input type="submit" name="submit" value="Iniciar Sesión">
    </form>
</body>
</html>
