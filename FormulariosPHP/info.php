<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    header('Location: login.php');
    exit();
}

$usuario = $_SESSION['usuario'];
$usuarios_registrados = isset($_SESSION['usuarios_registrados']) ? $_SESSION['usuarios_registrados'] : array();
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Información</title>
    <link rel="stylesheet" href="info.css">
</head>
<body>
    <h2>Información del usuario predefinido</h2>
    <table class="user-info">
        <tr>
            <td>Nombre:</td>
            <td><?php echo $usuario['nombre']; ?></td>
        </tr>
        <tr>
            <td>Fecha de Nacimiento:</td>
            <td><?php echo $usuario['fec_nac']; ?></td>
        </tr>
        <tr>
            <td>Número de Cuenta:</td>
            <td><?php echo $usuario['num_cta']; ?></td>
        </tr>
    </table>

    <h3>Listado de Usuarios Registrados</h3>
    <table class="user-list">
        <tr>
            <th>Número de Cuenta</th>
            <th>Nombre Completo</th>
            <th>Fecha de Nacimiento</th>
        </tr>
        <?php foreach ($usuarios_registrados as $usuario_registrado): ?>
            <tr>
                <td><?php echo $usuario_registrado['num_cta']; ?></td>
                <td><?php echo $usuario_registrado['nombre'] . ' ' . $usuario_registrado['primer_apellido']. ' ' . $usuario_registrado['segundo_apellido']; ?></td>
                <td><?php echo $usuario_registrado['fec_nac']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <a href="formulario.php">Ir a Formulario</a>
    <br>
    <a href="logout.php">Cerrar Sesión</a>
</body>
</html>

