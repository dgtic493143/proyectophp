<?php
//Tsutsumi Bernal David Israel

//Realizar una expresión regular que detecte emails correctos.
$email = "david@gmail.com";

if (preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/", $email)) {
    echo "Correo electrónico válido";
} else {
    echo "Correo electrónico inválido";
}

echo "<br />";
echo "<br />";
//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$curp = "ABCD123456EFGHIJ78";

if (preg_match("/^[ABCD123456EFGHIJ78]{18}$/", $curp)) {
    echo "CURP válido";
} else {
    echo "CURP inválido";
}

echo "<br />";
echo "<br />";
//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$frase = "Este texto tiene palabras con longitud mayor a 50 formada solo por letras o no?";

preg_match_all("/\b[a-zA-Z]{51,}\b/", $frase, $matches);

if (!empty($matches[0])) {
    echo "Palabras encontradas: " . implode(', ', $matches[0]);
} else {
    echo "No se encontraron palabras de longitud mayor a 50 formadas solo por letras.";
}

echo "<br />";
//Crea una funcion para escapar los simbolos especiales.
function escaparCaracteresEspeciales($cadena) {
    return htmlspecialchars($cadena, ENT_QUOTES, 'UTF-8');
}

echo "<br />";
echo "<br />";
// Ejemplo de uso:
$entrada = "<script>alert('Hola');</script>";
$salida = escaparCaracteresEspeciales($entrada);

echo "Entrada: $entrada<br>";
echo "Salida: $salida";

echo "<br />";
echo "<br />";
//Crear una expresion regular para detectar números decimales.
$numero_decimal = "123.45";

if (preg_match("/^\d+(\.\d+)?$/", $numero_decimal)) {
    echo "Es un número decimal";
} else {
    echo "No es un número decimal";
}

?>